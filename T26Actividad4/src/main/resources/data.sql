DROP TABLE IF EXISTS `reserva`;
DROP TABLE IF EXISTS `equipos`;
DROP TABLE IF EXISTS `investigadores`;
DROP TABLE IF EXISTS `facultad`;

CREATE TABLE facultad(
id int not null auto_increment,
nombre varchar(100),
PRIMARY KEY (id)
);

CREATE TABLE equipos (
id int not null auto_increment,
nombre varchar(100),
facultad int not null,
PRIMARY KEY (id),
FOREIGN KEY (facultad) REFERENCES facultad(id)
);

CREATE TABLE investigadores(
id int not null auto_increment,
nomapels varchar(100),
facultad int not null,
PRIMARY KEY (id),
FOREIGN KEY (facultad) REFERENCES facultad(id)
);

CREATE TABLE reserva (
id int not null auto_increment,
idinvestigadores int not null,
idequipos int not null,
comienzo varchar(100),
fin varchar(100),
PRIMARY KEY (id),
FOREIGN KEY (idInvestigadores) REFERENCES investigadores(id),
FOREIGN KEY (idEquipos) REFERENCES equipos(id)
);

insert into facultad(nombre) values('medicina');
insert into equipos(nombre, facultad) values('betis', 1);
insert into investigadores(nomApels, facultad) values('aitor alarcon', 1);
insert into reserva(idinvestigadores, idequipos, comienzo, fin) values(1,1,'4-9-2020', '5-9-2020');
