package com.example.demo.service;

import java.util.List;

import com.example.demo.dto.MaquinasRegistradas;

public interface IMaquinasRegistradasService {

	public List<MaquinasRegistradas> listarMaquinasRegistradas();
	
	public MaquinasRegistradas guardarMaquinas(MaquinasRegistradas maquinas);
	
	public MaquinasRegistradas maquinasRegistradasID(int id);
	
	public MaquinasRegistradas actualizarMaquina(MaquinasRegistradas maquinas);
	
	public void eliminarMaquinas(int id);
}
