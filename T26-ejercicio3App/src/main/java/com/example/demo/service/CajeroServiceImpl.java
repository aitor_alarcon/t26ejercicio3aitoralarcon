package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.ICajerosDAO;
import com.example.demo.dto.Cajeros;

@Service
public class CajeroServiceImpl implements ICajerosService{

	@Autowired
	ICajerosDAO iCajeroDAO;
	
	@Override
	public List<Cajeros> listarCajeros() {
		// TODO Auto-generated method stub
		return iCajeroDAO.findAll();
	}

	@Override
	public Cajeros guardarCajero(Cajeros cajero) {
		// TODO Auto-generated method stub
		return iCajeroDAO.save(cajero);
	}

	@Override
	public Cajeros cajeroID(int id) {
		// TODO Auto-generated method stub
		return iCajeroDAO.findById(id).get();
	}

	@Override
	public Cajeros actualizarCajero(Cajeros cajero) {
		// TODO Auto-generated method stub
		return iCajeroDAO.save(cajero);
	}

	@Override
	public void eliminarCajero(int id) {
		// TODO Auto-generated method stub
		iCajeroDAO.deleteById(id);
	}

}
