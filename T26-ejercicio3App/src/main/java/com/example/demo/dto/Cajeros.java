package com.example.demo.dto;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "cajeros")
public class Cajeros {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(name = "Nomapels")
	private String NomApels;
	
	@OneToMany
	@JoinColumn(name = "id")
	private List<Venta> venta;

	//Constructores
	public Cajeros() {
		
	}

	/**
	 * 
	 * @param id
	 * @param nomApels
	 * @param venta
	 */
	public Cajeros(int id, String nomApels, List<Venta> venta) {
		//super();
		this.id = id;
		this.NomApels = nomApels;
		this.venta = venta;
	}

	//Getters y Setters
	
	/**
	 * 
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * 
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * 
	 * @return NomApels
	 */
	public String getNomApels() {
		return NomApels;
	}
	
	/**
	 * 
	 * @param nomApels the nomApels to set
	 */
	public void setNomApels(String nomApels) {
		NomApels = nomApels;
	}

	/**
	 * 
	 * @return the venta
	 */
	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "Venta")
	public List<Venta> getVenta() {
		return venta;
	}

	/**
	 * 
	 * @param venta the venta to set
	 */
	public void setVenta(List<Venta> venta) {
		this.venta = venta;
	}

	@Override
	public String toString() {
		return "Productos [id=" + id + ", NomApels=" + NomApels + "]";
	}
	
	
}
