package com.example.demo.controller;

import java.awt.Cursor;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.Cajeros;
import com.example.demo.service.CajeroServiceImpl;

@RestController
@RequestMapping("/api")
public class CajerosController {

	@Autowired
	CajeroServiceImpl cajeroServiceImpl;
	
	@GetMapping("/cajeros")
	public List<Cajeros> listarCajeros() {
		return cajeroServiceImpl.listarCajeros();
		
	}
	
	@PostMapping("/cajeros")
	public Cajeros guardarCajero(@RequestBody Cajeros cajero) {
		return cajeroServiceImpl.guardarCajero(cajero);
		
	}
	
	@GetMapping("/cajeros/{id}")
	public Cajeros cajeroID(@PathVariable(name = "id") int id) {
		
		Cajeros cajero_id = new Cajeros();
		
		cajero_id = cajeroServiceImpl.cajeroID(id);
		
		System.out.println("Cajero ID: " + cajero_id);
		
		return cajero_id;
		
	}
	
	@PutMapping("/cajeros/{id}")
	public Cajeros actualizarCajero(@PathVariable(name = "id") int id, @RequestBody Cajeros cajero) {
		
		Cajeros cajero_sel = new Cajeros();
		Cajeros cajero_act = new Cajeros();
		
		cajero_sel = cajeroServiceImpl.cajeroID(id);
		
		cajero_sel.setNomApels(cajero.getNomApels());
		
		cajero_act = cajeroServiceImpl.actualizarCajero(cajero_sel);
		
		return cajero_act;
		
	}
	
	@DeleteMapping("/cajeros/{id}")
	public void eliminarCajero(@PathVariable(name = "id") int id) {
		cajeroServiceImpl.eliminarCajero(id);
	}
}
