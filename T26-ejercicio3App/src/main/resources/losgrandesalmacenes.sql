DROP TABLE IF EXISTS `venta` cascade;
DROP TABLE IF EXISTS `cajeros` cascade;
DROP TABLE IF EXISTS `productos` cascade;
DROP TABLE IF EXISTS `maquinas_registradas` cascade;

CREATE TABLE `cajeros` (
  `id` int AUTO_INCREMENT,
  `NomApels` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `maquinas_registradas` (
  `id` int AUTO_INCREMENT,
  `piso` int DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `productos` (
  `id` int AUTO_INCREMENT,
  `nombre` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `precio` int DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `venta` (
  `id` int AUTO_INCREMENT,
  `cajero` int NOT NULL,
  `maquina` int NOT NULL,
  `producto` int NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `venta_ibfk_1` FOREIGN KEY (`cajero`) REFERENCES `cajeros` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `venta_ibfk_2` FOREIGN KEY (`maquina`) REFERENCES `maquinas_registradas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `venta_ibfk_3` FOREIGN KEY (`producto`) REFERENCES `productos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
);

INSERT INTO `cajeros` (id, NomApels) values (1, 'Aitor Alarcón');
INSERT INTO `cajeros` (id, NomApels) values (2, 'Oscar Trillas');
INSERT INTO `cajeros` (id, NomApels) values (3, 'Alberto García');

INSERT INTO `maquinas_registradas` (id, piso) values (1, 1);
INSERT INTO `maquinas_registradas` (id, piso) values (2, 2);
INSERT INTO `maquinas_registradas` (id, piso) values (3, 3);

INSERT INTO `productos` (id, nombre, precio) values (1, 'Champú', 5);
INSERT INTO `productos` (id, nombre, precio) values (2, 'PC', 856.99);
INSERT INTO `productos` (id, nombre, precio) values (3, 'Teléfono móvil', 754.59);

INSERT INTO `venta` (id, cajero, maquina, producto) values (1, 1, 1, 1);
INSERT INTO `venta` (id, cajero, maquina, producto) values (2, 2, 2, 2);
INSERT INTO `venta` (id, cajero, maquina, producto) values (3, 3, 3, 3);

